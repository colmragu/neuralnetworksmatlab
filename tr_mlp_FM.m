%
% function  [FM_Update,dlog]=tr_mlp_FM(TSu,TSy,network,P, Ut,Yt, St)
%
% This algorithm trains the MLP network using Full Memory BFGS 
%
% TSu and TsY are the training set vectors 
% The network object contains the MLP weight matrices and flags are the network
% structure flags (type help NeuralNetwork for details)
%
% Ut and Yt are the test set 
%
%
% return parameters
% 
% FM_Update the network object of the trained neural network
% dlog log's of the training and testing of the FMBFGS process. 
%
% Currently implemented for single output networks only

function  [FM_Update,dlog]=tr_mlp_FM(TSu,TSy,network,P, Ut,Yt, St)

  % ================================================= %
  % Set up to defaults if not specified in input args %
  % ================================================= %

  n_iter=1000; RESET_FREQ=1000;  COST_GOAL=1.0e-10; SQE_BREAK=15;

  Params=[n_iter RESET_FREQ COST_GOAL];
  if nargin >5
     lenP=length(P);
     if lenP >3
        error('Invalid parameter vector length');
     end
     for j=1:lenP
        Params(j)=P(j);
     end
  end
  n_iter=Params(1);
  RESET_FREQ=Params(2); 
  COST_GOAL=Params(3);
 
  % Determine form of Model validation required
  
  ModValidate=0;   % none 
  if nargin ==7  %error
      error('Invalid number of input parameters');
  end
  if nargin ==8   % Test set validation only 
     ModValidate=1;
  end
  if nargin ==9   % Test set and Parallel model validation 
     ModValidate=2;
  end

  % ================================================= %

  % ================================================= %
  % initialise algorithm
  % ================================================= %
  
  %For linesearch diagnostics
  global Nmax;
  global Beta;
  global Acc;
  


  if isempty(Nmax)
     Nmax=2;
  end
  if isempty(Beta)
     Beta=0.6;
  end
  if isempty(Acc)
     Acc=10;
  end
  fprintf('\nLinesearch ... Accuracy : %5.3f, Nmax: %5.3f, Beta: %5.3f\n', Acc,Nmax,Beta);

  
  SQE=mlp_SQE(network,TSu,TSy);
  i=0;
  fprintf('\nIt.%3d :',i);
  Bgrad=mlp_Bgrad(network,TSu,TSy);

  fprintf(' MSE=%9f\n',SQE/length(TSy));
  [nc ni1]=size(network.w{1});
  [nc1 no]=size(network.w{2});
  ni=ni1-1; % extra weights for biases
  
  nw=size(Bgrad,1);

  % initialise loop variables

  e_cost=[];  v_cost=[];  p_cost=[]; 
  cond_log=[]; step_log=[]; MaxStep_log=[]; LSdata_log=[];
 
  e_validate=NaN;e_predict=NaN; 
  Cond_Num=NaN; neta=NaN;

  reset_count=1;  old_SQE=SQE;  stuck=0; SQEcount=1;
  loop=1; i=1; M=eye(nw); 
  

  % ================================================= %
  % optimisation loop
  % ================================================= %
  
  while (loop ==1) & (i<n_iter)


	% Compute search direction
   % ==================================== %

   M_grad=-M*Bgrad;
   


    
   LSslope=Bgrad'*M_grad;
	% Compute step size
   % ==================================== %
   
   [w]=vect2mlp(M_grad,network); %oo
    

    
    [neta SQE Nmax LSdata]=qd_ls_mlp(SQE,w,network,TSu,TSy, Nmax,Acc, 0,Beta,LSslope);

    if (i <2)
      if(Beta <1) 
         Nmax=2*neta;
      end
   end
      
   if neta == 0
       reset_count=RESET_FREQ+1;
   end
	
	% Compute s and update weights
   % ==================================== %

	s=neta*M_grad;
	% Update weights
	%network.w{1}=network.w{1}+neta*w{1};
	%network.w{2}=network.w{2}+neta*w{2};
    
    for j=1:network.layers+1    
        network.w{j}=network.w{j}+neta*w.w{j};
    end

    FM_Update=network;
	% Compute the new gradient and p
   % ==================================== %

	Bgrad_old=Bgrad;
 	fprintf('It.%3d :',i);
	Bgrad=mlp_Bgrad(network,TSu,TSy);
	p=Bgrad-Bgrad_old;
	fprintf(' MSE=%9f\n',SQE/length(TSy));
	% get new inverse Hessian (BFGS)
   % ==================================== %

	M=bfgsfm(M,p,s);
	reset_count=reset_count+1;  
    
	Cond_Num=cond(M);

  
   % End of Main part of the BFGD algorithm ......  
	% ====================================================== %

	% ====================================== %
   %        Model Validation tests          %
	% ====================================== %
        
        if ModValidate > 0   
           % Test set validation
           if ModValidate >1 
               % Parallel model validation (Test set)
               [yp yd res]=predMLP([Yt Ut],St,network,1,1);
               e_predict=(res)'*(res);
           end
        end
   e_validate=mlp_SQE(network,Ut,Yt);

  	% ====================================== %

   % ========================== %
  	%        Data Logging        %
	% ========================== %
 
	% Data to be returned

	e_cost=[e_cost;SQE];
	v_cost=[v_cost;e_validate];
	p_cost=[p_cost;e_predict];

	cond_log=[cond_log; Cond_Num];
	step_log=[step_log;neta];
   MaxStep_log=[MaxStep_log;Nmax];
   LSdata_log=[LSdata_log; LSdata];

   % Data to be written to file ... none at present

	% print intermediate results to screen
	% if rem(i,1)==0
	%	[i;neta;SQE;Cond_Num;e_validate]'
	% end

   % ==================================== %

	% ====================== %
        %   Stopping Criteria    %
  	% ====================== %

        % .... if stuck in a local minimum
	if reset_count>RESET_FREQ
		M=eye(nw);
		reset_count=1;

      if neta==0
		  stuck=stuck+1;
		end
                                
		if stuck>5
		   fprintf('\nstuck at local minimum\n');
	      loop=0;
		end
	 else
      stuck=0;
    end

	% .... if SQE reduction is to small

	% Reset M if SQE reduction is poor
   % ==================================== %

	if (old_SQE-SQE)<old_SQE*0.0005
	   SQEcount=SQEcount+1;
	else
	  old_SQE=SQE;
	  SQEcount=1;
	end
    
        if SQEcount > SQE_BREAK
           loop=0;
           fprintf('Error reduction to small\n');
	end

	
	% .... if Error goal is achieved
 
	if SQE<COST_GOAL
	   loop=0;
	end

        %increment loop
      	i=i+1;
        
       	% ==================================== %
	% ==================================== %
  end
  
  % Form dlog
  % ==================================== %
 

  
  dlog=[e_cost v_cost p_cost step_log MaxStep_log cond_log LSdata_log];
  fprintf('\n\n');
return

	


