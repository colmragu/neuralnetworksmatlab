%
%  [v]=mlp2vect(network)
%
%   Transforms the MLP parameters stored in standard network object
%   to the vector form used in BFGS and other gradient based
%   training algorithms;
%

function [v]=mlp2vect(network)

   v=[];

   for i=1:network.layers+1
       v=[v; network.w{i}(:)];
   end
   

%end


