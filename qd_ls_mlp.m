function [nbest,ebest, Nmax, LSdata]=qd_ls_mlp(SQE_0,w,network,u,y,Nprev,Acc, flag, Beta, WolfeC1)

% function [nbest,ebest, Nmax,LSdata]=qd_ls_mlp(SQE_0,w,network,u,y,Nprev,Acc, flag, Beta, WolfeC1)
%
% perform a quadratic line search in the direction given by w
% returning the optimum step size and corresponding error.
%
% The initial baracket is given by [0 2*Nprev], with the cost function 
% for n=0 given by SQE_0. The algotithm works on the basis that the
% search direction is a gradient  decent direction, hence there must 
% be a point greater than zero which has a lower cost than n=0. If such
% a point is not found then a minimum is assumed.
%
% Acc determines the accuracy with which the minimum is obtained
% Defaults to exact (10) if not specified.. Specified as max no. of quad fits
%
% Npev is used in the determination of the initial guess for the bracket
% to search for a minimum. For the first iteration it defaults to N_MAX
% after which previous step sizes should be returned in Nprev. 
% If not specified at all theb N_MAX is always used as an the first guess
%
% flag : used to display diagnostics flag =1 : graph + data flag =2 dat only.
% Beta : Stiffness factor if present , 0.6 is default

  N_MAX=2;       	% Initial value for Nmax
  MAX_NQUAD=10;  	% Maximum number of quadratic fits
  MAX_NMBRAK=10; 	% Maximum number of braket magnifications allowed
  MAX_NRBRAK=25;	% Maximum number of braket reductions used in searching for a minimum
  MAX_NFUNC=20; 	% Maximum number of function evaluation in total

  CGOLD=1/0.381966;	% golden ratio scaling factor for magnification
  tol=10*sqrt(eps);
  if nargin <5
     error('Incorrect number of arguments');
  end
  Nmax=N_MAX;
  if (nargin >5)
    if ~isempty(Nmax) 
      Nmax=Nprev;
    end
  end

  %if (nargin >3)
  %  MAX_NQUAD=Acc-1;
  %end  

  if (nargin <8)
     flag=0;
  end
  if (nargin <9)
     Beta=0.6;
  end
  if (Beta <0 | Beta >1)
    error('Beta must be between 0 and 1');
  end
  if (nargin <10)
     WolfeC1=NaN;
  end
  

   % initial values for the bracket
   % ==================================== %
   n=[0 0 0]; e=[SQE_0 SQE_0 SQE_0];  				   
   nbest=n(1); ebest=e(1);

   
   n(2)=Nmax;



   e(2)=ev_mlp_s(n(2),w,network,u,y);  



   
   WC=-1;
   if ((SQE_0 + 0.05*n(2)*WolfeC1-e(2)) >0)
      WC=1;
   end

   
   [nbest ebest]=neta_best(n(2),e(2),nbest,ebest);
   n(3)=n(2)/2; 
  


   if (flag ==1)
     ek=[]; nk=[];
     for k=-Nmax/10:Nmax/10:Nmax
        eg1=ev_mlp_s(k,w,network,u,y);
        ek=[ek;eg1];
        nk=[nk;k];
     end 
     plot(nk,ek);
    grid;
    pause(0.5)
  end
   

   % search for minimum .. quadratic line search

   Nfunc=1; NRbrak=0; stop=0;
   while (stop==0)
      
      % Evaluate midpoint of the bracket
      % ==================================== %
	
      e(3)=ev_mlp_s(n(3),w,network,u,y);
      %e
      Nfunc=Nfunc+1;
      [nbest ebest]=neta_best(n(3),e(3),nbest,ebest);
      if ((SQE_0 + 0.05*n(3)*WolfeC1 - e(3) ) >0)
         if (WC <0)
            WC= Nfunc;
         end
      end



      % If error is monotonically decreasing increase 
      % bracket interval until a concave interval is obtained  
      % ==================================== %
   %n
   %e
      NMbrak=0;
      while ( (e(3)<e(1)) & (e(2)<e(3)) & stop==0)  
         % Comment these two lines out to fix n(0) as the initial squared error
         % Otherwise bracket will close in on minimum from either side
         % n(1)=n(3);
         % e(1)=e(3);
         % End of optional code
         
         n(3)=n(2);  
         e(3)=e(2);
         n(2)=CGOLD*n(2);  
         e(2)=ev_mlp_s(n(2),w,network,u,y);
         [nbest ebest]=neta_best(n(2),e(2),nbest,ebest); Nfunc=Nfunc+1;
         if((SQE_0+0.05*n(2)*WolfeC1-e(2)) >0)
            if (WC <0)
               WC= Nfunc;
            end
         end

         NMbrak=NMbrak+1;
         if (Nfunc >MAX_NMBRAK+2)
            stop=1;
         end
         if (flag >0)
            fprintf('Yahoo ..\n')
         end
      end

      % If a concave region exists find the minimum using a quadratic line search
      % ==================================== %
      NQuad=0;
      if ((e(3)<e(2)) & (e(3)<e(1)) & stop==0)

         nopt=0;
         while (stop==0)
            NQuad=NQuad+1;
            nopt_old=nopt;
            nopt=quadmin(n,e);
            eopt=ev_mlp_s(nopt,w,network,u,y);
            [nbest ebest]=neta_best(nopt,eopt,nbest,ebest); Nfunc=Nfunc+1;
            if((SQE_0+0.1*nopt*WolfeC1-eopt) >0)
               if (WC <0)
                  WC= Nfunc;
               end
            end

            % close in on minimu
            if eopt<e(3)

               if nopt<n(3)
                  n(2)=n(3); e(2)=e(3);
               elseif nopt>n(3)
                  n(1)=n(3); e(1)=e(3);
               end
               n(3)=nopt; e(3)=eopt;
            elseif eopt>e(3)
               if nopt<n(3)
                  n(1)=nopt; e(1)=eopt;
               elseif nopt>n(3)
                  n(2)=nopt; e(2)=eopt;
               end
            end

            % Check accuracy and stop if tolerance limits reached
            % ================================================= %

            if abs(nopt-nopt_old) < tol
               stop=1;
               if (flag >0)
                  fprintf('Exact min ..');
               end
            end
            if NQuad > MAX_NQUAD
               stop=1;
            end
            if (Nfunc >MAX_NFUNC)
               stop=1;
            end
         end
      end

      % Otherwise suitable braket not found - try smaller region
      % ==================================== %
      if (stop~=1)
         n(2)=n(3); e(2)=e(3);
         n(3)=n(2)/2;
         NRbrak=NRbrak+1;
      end

      % ==================================== %
      % if count is large then must be at a local minimum

      if Nfunc > MAX_NFUNC
         stop=1;
      end
 
      if NRbrak > MAX_NRBRAK
         stop=1;
      end
   end

   % Compute new value of Nmax
   % ==================================== %
	
     % Stiffness factor Beta (0-1)	
   if nbest ~= 0
       Nmax= (Beta)*(Nprev)+(1-Beta)*2*nbest;  % Moving average estimate of neta_max
   end
   if (flag >0)
     fprintf('%d %d %d %d ' ,Nfunc, NRbrak, NMbrak, NQuad);
   end
   LSdata=[Nfunc NRbrak NMbrak NQuad WolfeC1 WC];
   
 %end
 %NQUAD= number of quadratic fits
 %NMBRAK = number of braket magnifications allowed
 %NRBRAK= number of braket reductions used in searching for a minimum
 %NFUNC= number of function evaluation in total


     
