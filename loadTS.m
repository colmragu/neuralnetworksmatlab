%function [TsU,Yd] = loadTS(name)
%
%Loads training set from file name.ts
%
%
%   TSU = Nv  x Ni matrix
%   Yd =  Nv  x No matrix
%
% See also createTS, saveTS and makeDel

function [TsU,Yd] = loadTS(name)


    if(isstr(name) ~=1)
        error('Error : must be a string');
    end

    fprintf('Loading Training set... ');


    Fext= '.ts';
    Fpath='./';
    fullname=[Fpath name Fext];
    fid=fopen(fullname, 'r');

    RBFname=fscanf(fid, '%s',1);
    Ninp=fscanf(fid,'%d',1);
    Nout=fscanf(fid,'%d',1);
    Nvec=fscanf(fid,'%d',1);

 
    
    TS=fscanf(fid,'%g',[Ninp+Nout Nvec]);
    TsU=TS(1:Ninp,:)';             % Transposed to column format 
	
    Yd=TS(Ninp+1:Ninp+Nout,:)'; 

    fprintf('TS(%d x [%d,%d]) loaded\n',Nvec,Ninp,Nout);


    fclose(fid);

%end
