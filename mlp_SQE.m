%
% [sqe] = mlp_SQE(network,TSu,TSy)
%
% the function returns the mlp network cost function. 
% The network object contains the weight and flag information and the MLP network parameters 
% u is the input matrix, y the output vector
% 
% Can make function conpute the percentage mean square error
% instead by defining a global variable PMSEflag and setting it to 1
%

function [sqe] = mlp_SQE(network,TSu,TSy)
 
	global PMSEflag;

	[y ,r]=mlpnet(network,TSu);
	err=y{network.layers}-TSy;
    
    %fprintf('mlp_SQE sizes \n')
    %size(network.w{2})
	
	sqe=err(:)'*err(:);
  
	if ~isempty(PMSEflag)
	  if PMSEflag ==1
		sqe=sqe/length(err(:))*100;  % percentage Mean squared error;	
	  end
	end
return
