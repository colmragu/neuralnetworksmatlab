% function [y,r]=mlpnet(network,u)
%
% Simulates a general 9 hidden layer or less MLP network - The network
% object contains the weight and MLP network parameters
% returning the outputs and regressor matrix for a given set of weights .
%
% Can run in online (1 input at a time) or batch mode ( where
% complete training set is given in u)
%
%	NB: Currently only sigmoid nonlinearties and linear output neurons
%	are supported.

function [y,r]=mlpnet(network,u)

    if flag(1) ~= 1
       error('Tanh neurons not implemented');
    end
    if flag(2) ~= 1 
       error('Only linear output neurons currently implemented');
    end
    
    y=cell(network.layers,1);
    r=cell(network.layers-1,1);
    [nv ni]=size(u);
    [nh ni1]=size(network.w{1}); %nh=network.Nh(1) ni1=network.Ni
    y{network.layers}=zeros(nv,network.No);
    for i=1:network.layers

        r{i}=ones(nv,network.Nh(i));
    end

    u1=ones(ni+1, nv);
    u1(2:ni+1,:)=u';

    %size(network.w{1})
    %size(u1)
    r{1}=sig(network.w{1}*u1);
    r{1}=[ones(1,size(r{1},2)); r{1}];
    %r{1}=r{1}';
    
    if (network.layers==2)
        %r{1}=r{1}';
    end
    
    for i=2:network.layers

       %i
       %size(r{i-1})
       %size(network.w{i})

       r{i}=sig(network.w{i}*r{i-1});
       r{i}=[ones(1,size(r{i},2));r{i}];   
       

 
       %size(r{i})
       %size(network.w{i+1})


    end



    
    y{network.layers}=network.w{network.layers+1}*r{network.layers};
    y{network.layers}=y{network.layers}';
    