%
%   [w]=vect2mlp(v,network)
%
%   Transforms the mlp parameters stored in vector form
%   to the standard network object form.
%   

function [w]=vect2mlp(v,network)

   lv=length(v);
   %if lv ~= (ni+1)*nh +(nh+1)*no;
   %    error('input vector dimension does not agree with MLP demsnions');
   %end
  
   w=network;
   pos_count=1;

   % convert vector to matrix form for w1 and w2;
   for i=1:network.layers+1
      size_of_layer=size(network.w{i},1);
      for j=1:size(network.w{i},2)
           pos_count+size_of_layer-1;
           
           w.w{i}(:,j)=v(pos_count : pos_count+size_of_layer-1);
            
         
           pos_count=pos_count+size_of_layer;

      end
      
      %remove for multiple layres

 

       
   end

return

