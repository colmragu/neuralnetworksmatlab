%function [nsg] = saveMLP(name,w1,w2,flags)
%
%Save MLP network information to a file 'name'.mlp
%
%See also createMLP and  loadMLP

function [nsg] = saveMLP(name,network)


    if(isstr(name) ~=1)
        error('Error : must be a string');
    end

    Fext= '.mlp';
    Fpath='./';

    fullname=[Fpath name Fext];
    fid=fopen(fullname, 'w');

    fprintf(fid, '%s\n',name);
    fprintf(fid,'\n');
    
    [nh ni1]=size(network.w{1});
    [nh1 no]=size(network.w{2});
    ni=ni1-1;   %Matrix dimension inlucdes bias

    fprintf(fid,'%d\n',ni);
    fprintf(fid,'%d\n',nh);
    fprintf(fid,'%d\n',no);
    fprintf(fid,'\n');

    %fprintf(fid,'%d\n',network.flags(1));
    %fprintf(fid,'%d\n',network.flags(2));
    fprintf(fid,'\n');
    %fprintf(fid,'%d\n',network.flags(3));
    fprintf(fid,'\n');

    Fdoub= '%9.8g   ';
    for i=1:ni-1 
        Fdoub =[Fdoub '%9.8g   '];
    end
    Fdoub = [Fdoub '\n'];


    Fdoub= '%9.8g   ';
    for i=1:nh1 
        Fdoub =[Fdoub '%9.8g   '];
    end
    Fdoub = [Fdoub '\n'];
    for i=1:network.layers+1
        fprintf(fid,Fdoub,network.w{i});
        fprintf(fid,'\n');

    end
   


    fclose(fid);

%end
