% function [y ,r]=mlpnet(w1,w2,flags,u)
%
% Simulates a general single hidden layer MLP network - MLP(ni,nh,no)
% returning the outputs and regressor matrix for a given set of weights .
%
% Can run in online (1 input at a time) or batch mode ( where
% complete training set is given in u)
%
%      u = nv x ni      - input vector (or matrix)
%      y = nv x no       - vector of outputs (single  output assumed)
%      r = nv x nh+1    - regressor matrix (Used for SVD etc .)
%      w1 = nh x (ni+1) - hidden layer weights
%
%      w2= (nh+1) x no  - Output layer weights (transposed for SVD)
%
%      flags =[f1 f2 f3],	f1 = 0/1  Tanh/Sigmoid nonlinearities
%      f2 = 0/1  nonlinear/linear output layer neurons 
%		 f3 = No. of training iterations performed (network status)
%
%	NB: Currently only sigmoid nonlinearties and linear output neurons
%	are supported.

function [y,r]=mlpnet(network,u)

    if flag(1) ~= 1
       error('Tanh neurons not implemented');
    end
    if flag(2) ~= 1 
       error('Only linear output neurons currently implemented');
    end
    
    y= cell(network.layers);
    r=cell(network.layers);
    [nv ni]=size(u);
    [nh ni1]=size(network.w{1});
    
    for i=1:network.layers

        u1{i}=ones(ni+1, nv);
        r{i}=ones(nh+1, nv);
    end

    u1=ones(ni+1, nv);
    u1(2:ni+1,:)=u';


    r{1}(2:nh+1,:)=sig(network.w{1}*u1);
    r{1}=r{1}';
    y{network.layers}=r{1}*network.w{2};
    
%end
