%
%  [w1,w2]=vect2mlp(vect,ni,nh,no)
%
%   Transforms the mlp parameters stored in vector form
%   to the standard w1,w2 form.
%   

function [w]=vect2mlp(v,network)

   lv=length(v);
   if lv ~= (network.Ni+1)*network.Nh +(network.Nh+1)*network.No;
       error('input vector dimension does not agree with MLP demsnions');
   end
   for j=1:(network.layers-1)
    w{network.layers}=zeros(network.Nh,network.Ni+1);
   end
   w{2}=zeros(network.Nh+1,network.No);
  
   % convert vector to matrix form for w1 and w2;
   for i=1:(network.Ni+1)
        w{1}(:,i)=v( 1+(i-1)*network.Nh : i*network.Nh);
   end
   for j=2:(network.layers-1)
    for i=1:(network.Ni+1)
        w{j}(:,i)=v( 1+(i-1)*network.Nh : i*network.Nh);
    end
   end
   v=v((network.Ni+1)*network.Nh+1:lv);

   for i=1:network.No
     w{network.layers}(:,i)=v( 1+(i-1)*(network.Nh+1) : i*(network.Nh+1));
   end
return

