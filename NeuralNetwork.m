% The network object contains the weight and flag information and the MLP network parameters
% This class initalizes this object
%
%
% function obj = NeuralNetwork(Ni,Nh,No,layers)
%
% Ni is the number of inputs to the network. 
% Nh is the number of Neurons in the hidden layer, note for multiple hidden
% layarers Nh must be a vector. 
% No is the number of outputs 
% layers is the number of hidden layers
% returns the network object.

classdef NeuralNetwork  

        properties
            w=cell(10);
            Ni;
            Nh;
            No;
            layers;
            flags;
           
        end
        

        methods

        
         function obj = NeuralNetwork(Ni,Nh,No,layers)
                
                if nargin == 3
                    fprintf('Creating MLP network ... ');
                  	obj.w{1}=2*(1-2*rand(Nh,Ni+1))*(3/sqrt(Ni));
                    obj.w{2}=(1-2*rand(Nh+1,No));

                    
                    obj.Ni=Ni;
                    obj.Nh=Nh;
                    obj.No=No;
                    obj.layers=2;
                end

                
                if nargin == 4
                    obj.w{1} =(1-2*rand(Nh(1),Ni+1));

                    for count=2:layers
                       obj.w{count} =(1-2*rand(Nh(count),Nh(count-1)+1));
                    end
                    
                    obj.w{layers+1} =(1-2*rand(No,Nh(layers)+1));

                    
                    obj.layers=layers;
                    obj.Ni=Ni;
                    obj.Nh=Nh;
                    obj.No=No;
                end
         end
         

            

      
            
       end
  
end