%
% function  [PSO_Update,dlogs,PSO_Best_TestSet]=tr_mlp_PSO(TSu,TSy,network, NoP,n_iter,Ut,Yt)
%
% This algorithm trains the MLP network using Particle swarm optimization. 
%
% TSu and TsY are the training set vectors 
% network is an object that contains the inital size of the network to be trained
% for structure of the network object (type help NeuralNetwork for details)
% NoP is the number of particle's in the swarm.
% n_iter is the number of itterations that the algorithm will run for. 
% Ut and Yt are the test data sets
%
% Return:
% PSO_update the network weights at the end to training. 
% dlog log's of the training and testing process.
% PSO_Best_TestSet the network weights at the point where the error of the
% trainging set and the test set were lowest. 
%
% Currently implemented for networks up to 9 layers

function  [PSO_Update, dlogs,PSO_Best_TestSet]=tr_mlp_PSO_vel_limit(TSu,TSy,network, NoP,search_space_offset,n_iter,Ut,Yt)

  % ================================================= %
  % Set up to defaults if not specified in input args %
  % ================================================= %
best_known_cost=100000000;
%n_iter=1000;
loop=1;  
k=0;
SQE=zeros(NoP,1);
P_best_cost=ones(NoP,1);
c=0.7298;
c1=1.4962;
c2=c1;

search_space_up=max(max([TSu,TSy]));
search_space_lo=min(min([TSu,TSy]));


%P=zeros(NoP,1);
%P_best=ones(NoP,1);
%Velocity=ones(NoP,1);
%Velocity_update=ones(NoP,1);





  e_cost=[];  v_cost=[];  p_cost=[]; 
  cond_log=[]; step_log=[]; MaxStep_log=[]; LSdata_log=[];
  
   e_validate=NaN;e_predict=NaN; 
  Cond_Num=NaN; neta=NaN;
  
Best_Known= NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);
PSO_Update= NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);
PSO_Best_TestSet=NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);


for i=1:NoP  %init particles.
    P(i)= NeuralNetwork(network.Ni,network.Nh,network.No,network.layers); 
    
    for j=1:NoP/2  %init particles.
        for k=1:network.layers
            [a,b]=size(P(i).w{k});
%            P(i).w{k}=normrnd(search_space_lo,search_space_up,size(P(i).w{k}));
                    P(i).w{k}=normrnd(search_space_lo,search_space_up,size(P(i).w{k}));


        end
    end

    
        for j=NoP/2:NoP  %init particles.
        for k=1:network.layers
            [a,b]=size(P(i).w{k});
%            P(i).w{k}=normrnd(search_space_lo,search_space_up,size(P(i).w{k}));
                        P(i).w{k}=normrnd(search_space_lo,search_space_up,size(P(i).w{k}));


        end
    end

    
   % for j=network.layers/2:network.layers
   %     P(i).w{j}=P(i).w{j}*-100;
   % end
    
    P_best(i)=P(i);
    Velocity(i)=NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);
    for j=1:network.layers+1
            [a,b]=size(P(i).w{k});
            Velocity(i).w{k}=zeros(a,b);

            
    end
    SQE(i) = mlp_SQE(P(i),TSu,TSy);
    P_best_cost(i)=SQE(i);
  
    if(SQE(i)<best_known_cost)    
        Best_Known.w=P(i).w;
        best_known_cost=SQE(i);
    end

    

end




while(loop ==1) & (k<n_iter)

           

    
    for i=1:NoP


        %SQE(i) = mlp_SQE(P(i),TSu,TSy);
        for j=1:network.layers+1
            
            a=rand(1,1);
            b=rand(1,1);
            
            Velocity_update(i).w{j}=c1*a*(P_best(i).w{j}-P(i).w{j})+(c2*b*(Best_Known.w{j}-P(i).w{j})); %particel update
            Velocity(i).w{j}=c*Velocity(i).w{j}+Velocity_update(i).w{j};
            %Velocity(i).w{j}=Velocity_update(i).w{j};
            %            while (max(max(Velocity(i).w{k}))>=2)
            %    for j=1:size(Velocity(i).w{k},1)
            %        for k=1:size(Velocity(i).w{k},2)
            %            if (Velocity(i).w{k}(i,j)>=2)
            %                Velocity(i).w{k}(i,j)=2;
            %                fprintf('over acc \n')
            %            end
            %        end
            %    end
            %end
            
            P(i).w{j}=P(i).w{j}+(Velocity(i).w{j});
            
            %max(Velocity_update(i).w{1})


        
        end
        

        SQE(i)=mlp_SQE(P(i),TSu,TSy);




        if(SQE(i)<P_best_cost(i))    
            %best_known_cost=SQE(i);
            %P(i).w=P(i).w+neta(i);Nmax
            
            P_best(i).w=P(i).w;
            P_best_cost(i)=SQE(i);
            

        end

        if(SQE(i)<=best_known_cost)

             
             Best_Known.w=P(i).w;
             best_known_cost=SQE(i);
             %fprintf('particle %3d is at the local minimum \n',i) 
        end
             
        
        if(max(SQE)-best_known_cost<0.00001)
                 %fprintf('error reduction too small\n');
                 fprintf('\nconverged\n')
                 loop=0;
                 break;
                 %loop=0;
        end
        
    end
        

        

        


    e_validate=mlp_SQE(Best_Known,Ut,Yt);
    
    if (e_validate<best_known_cost)
        PSO_Best_TestSet.w=Best_Known.w;
    end
    
    e_cost=[e_cost;best_known_cost];
    v_cost=[v_cost;e_validate];

%	p_cost=[p_cost;SQE(2)];
%	cond_log=[cond_log; SQE(3)];
%	step_log=[step_log;neta];
%    MaxStep_log=[MaxStep_log;Nmax];
%    LSdata_log=[LSdata_log; LSdata];
 
  
    
    k=k+1;
    fprintf('It.%3d :',k);
    fprintf(' MSE=%9f\n',best_known_cost/length(TSy));
    %for(j=1:NoP)
        %fprintf(' Particle.%3d :', j);
        %fprintf('SQE=%9f\n',SQE(j)/length(TSy));
    %end
    if(max(SQE(i))==best_known_cost)
          %loop_count=loop_count+1;
          %P(1)= NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);
    end



end



PSO_Update.w=Best_Known.w;
%PSO_Update.dlog=best_known_cost;
dlogs=[e_cost v_cost p_cost step_log MaxStep_log cond_log LSdata_log];

return

	


