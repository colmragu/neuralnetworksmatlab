%
% function [grad] = mlp_Bgrad(w1,w2,flags,TSu,TSy)

% the function returns the vector gradient of the MLP network 
% cost function.

function [grad] = mlp_Bgrad(network,u,yd)

   global PMSEflag;

   [nh ni1]=size(network.w{1});
   [nv ni]=size(u);
   [no nh1]=size(network.w{2});

   grad_w1=zeros(nh,ni1);
   grad_w2=zeros(nh1,no);
   row_1=ones(nv,1);
   
   grad=NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);
   
 
   [y,r]=mlpnet(network,u);


   err=y{network.layers}-yd;  		% (nv *  no) matrix


 % NB: Using PMSE gradient scaling factor leads to rounding errors 
 
 if ~isempty(PMSEflag)
    if PMSEflag ==1
 	   err=err/length(err(:))*100;% normalised if % Mean squared error used;	
 	 end
end	

  %r{1}=r{1}'; 		% Tranposed for use below (nh1 *nv) matrix

   %size(err)
   %size(r{1})

   grad_w2= r{1}*err;	% nh1 *no matrix	

   %compute factors independant of Nv
   
   %For sig function dr = r(1-r)

   r{1}=r{1}.*(1-r{1});
   r{1}=r{1}(2:nh1,:);		% NB r is now dr and is a (nh *nv) matrix ....
   
   u=[row_1 u];		% Add row of ones for bias weights
   %network.w{2}=network.w{2}';

   %size(network.w{2}(:,2:nh1))
   %size(err)
   P=network.w{2}(:,2:nh1)'*err';	% (nh * nv) matrix

   %size(P)
   %P=P';

   grad_w1= (P.*r{1})* u;
   %size(grad_w1)


   gradw1=mean(abs(grad_w1(:)));
   gradw2=mean(abs(grad_w2(:)));
   

  
  
   %fprintf('  gw1=%9f, gw2=%9f ',gradw1,  gradw2);
   %grad_weights=NeuralNetwork(grad_w1,grad_w2);
   %grad=mlp2vect(grad_weights);
   
   grad.w{1}=grad_w1;
   grad.w{2}=grad_w2;
   
   
   grad=mlp2vect(grad);
   

return

%multiple layers
