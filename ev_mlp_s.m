%
% [cost] = ev_mlp_s(neta,w,network,u,y)
%
% the function returns the MLP network cost function.
% for step_size neta and gradient direction w.  
% u is the input matrix, y the output vector, and network object contains the current weight's and  
% MLP parameters.



function [cost] = ev_mlp_s(neta,w,network,u,y)
   
    NeuralNetworkUpdate=NeuralNetwork(network.Ni,network.Nh,network.No,network.layers);    
    % Compute new weights
    for i=1:network.layers+1    
        NeuralNetworkUpdate.w{i}=network.w{i}+neta*w.w{i};
        
    end
    
    % Calc the SQE ..
    cost=mlp_SQE(NeuralNetworkUpdate,u,y);
   
%end



