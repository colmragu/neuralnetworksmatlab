function [qmin]=quadmin(x,y)

% [qmin]=quadmin(x,y)
%
%  returns the minimum to the quaratic fitting the three points
%  in x,y

  num=(y(1)-y(3))*(x(3)-x(2))*(x(2)-x(1)) ;
  denom=(x(2)-x(3))*y(1)+(x(3)-x(1))*y(2)+(x(1)-x(2))*y(3) ;

  if denom==0
	if (y(1)<y(2) & y(1)<y(3)) 
		qmin=x(1);
	end
        if (y(2)<y(1) & y(2)<y(3)) 
		qmin=x(2);
        else 
		qmin=x(3);
	end
  else 
	qmin=(x(1)+x(3)-(num/denom))/2;
  end
%end
