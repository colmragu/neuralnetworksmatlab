%
% function  BBP_Update=tr_mlp_BBP(TSu,TSy,network,P, Ut,Yt, St)
%
% This algorithm trains the MLP network using the BBP training algorithm 
%
% TSu and TsY are the training set vectors 
% w1 and w2 are the inital MLP weight matrices and flags are the network
% structure flags (type help mlpnet for details)
%
% Ut and Yt are the test set 
%
% Optional input parameters
% P=[n_iter reset_freq  cost_goal], Ut , Yt, St
%
% Optional return parameters
% 
% w1_opt, w2_opt and  dlog= [SQE stepsize er_predict]
%
% Currently only implemented for single output networks

function  BBP_Update=tr_mlp_BBP(TSu,TSy,network,P, Ut,Yt, St)

  % ================================================= %
  % Set up to defaults if not specified in input args %
  % ================================================= %

  n_iter=1000; RESET_FREQ=1000;  COST_GOAL=1.0e-10; SQE_BREAK=15;
  
  %NB RESET_FREQ does not apply in the BBP algroithm, but is left in for consistancy with 
  %the other training algoithms
  
  Params=[n_iter RESET_FREQ COST_GOAL];
  if nargin >5
     lenP=length(P);
     if lenP >3
        error('Invalid parameter vector length');
     end
     for j=1:lenP
        Params(j)=P(j);
     end
  end
  n_iter=Params(1);
  RESET_FREQ=Params(2); 
  COST_GOAL=Params(3);
 
  % Determine form of Model validation required
  
  ModValidate=0;   % none 
  if nargin ==7  %error
      error('Invalid number of input parameters');
  end
  if nargin ==8   % Test set validation only 
     ModValidate=1;
  end
  if nargin ==9   % Test set and Parallel model validation 
     ModValidate=2;
  end

  % ================================================= %

  % ================================================= %
  % initialise algorithm
  % ================================================= %
  
    
  %For linesearch diagnostics
  global Nmax;
  global Beta;
  global Acc;
  
  if isempty(Nmax)
     Nmax=0.1;
  end
  if isempty(Beta)
     Beta=0.6;
  end
  if isempty(Acc)
     Acc=10;
  end
  fprintf('\nLinesearch ... Accuracy : %3.0f, Nmax: %5.4f, Beta: %3.1f\n', Acc,Nmax,Beta);

  
  
  SQE=mlp_SQE(network,TSu,TSy);
  i=0;
  fprintf('\nIt.%3d :',i);
  Bgrad=mlp_Bgrad(network,TSu,TSy);
  fprintf(' MSE=%9f\n',SQE/length(TSy));
  [nc ni1]=size(network.w{1});
  [nc1 no]=size(network.w{2});
  ni=ni1-1; % extra weights for biases
  
  nw=size(Bgrad,1);
 
  % initialise loop variables

  e_cost=[];  v_cost=[];  p_cost=[]; 
  cond_log=[]; step_log=[]; MaxStep_log=[]; LSdata_log=[];
 
  e_validate=NaN;e_predict=NaN; 
  Cond_Num=NaN; neta=NaN;

  reset_count=1;  old_SQE=SQE;  stuck=0; SQEcount=1;
  loop=1; i=1; M=eye(nw); 
  
  

  % ================================================= %
  % optimisation loop
  % ================================================= %
  
  while (loop ==1) & (i<n_iter)


   % Compute search direction
   % ==================================== %

	M_grad=-Bgrad;

   LSslope=Bgrad'*M_grad;
	% Compute step size
   % ==================================== %
	[w]=vect2mlp(M_grad,network);

 	[neta SQE Nmax LSdata]=qd_ls_mlp_BBP(SQE,w,network,TSu,TSy, Nmax,Acc, 0,Beta,LSslope);
    
   % XXX: Used with qd_ls_mlp() but not qd_ls_mlp_BBP() 
   if (i <2)
      if(Beta <1) 
         Nmax=2*neta;
      end
   end
   % End of XXX
    
   if neta == 0
      reset_count=RESET_FREQ+1;
   end
	
   % update weights
   % ==================================== %


	% Update weights
    for j=1:network.layers    
        network.w{j}=network.w{j}+neta*w{j};
    end

	
    BBP_Update=network;

   % Compute the new gradient
   % ==================================== %

	Bgrad_old=Bgrad;
 	fprintf('It.%3d :',i);
	Bgrad=mlp_Bgrad(network,TSu,TSy);
	
	fprintf(' MSE=%9f\n',SQE/length(TSy));


   % End of Main part of the BFGD algorithm ......  
	% ====================================================== %

	% ====================================== %
   %        Model Validation tests          %
	% ====================================== %
        
        if ModValidate > 0   
           % Test set validation
           e_validate=mlp_SQE(w1,w2,flags,Ut,Yt);
           if ModValidate >1 
               % Parallel model validation (Test set)
               [yp yd res]=predMLP([Yt Ut],St,w1,w2,flags,1,1);
               e_predict=(res)'*(res);
           end
        end

  	% ====================================== %

   % ========================== %
  	%        Data Logging        %
	% ========================== %
 
	% Data to be returned

	e_cost=[e_cost;SQE];
	v_cost=[v_cost;e_validate];
	p_cost=[p_cost;e_predict];

	cond_log=[cond_log; Cond_Num];
	step_log=[step_log;neta];
   MaxStep_log=[MaxStep_log;Nmax];
   LSdata_log=[LSdata_log; LSdata];

   % Data to be written to file ... none at present

	% print intermediate results to screen
	% if rem(i,1)==0
	%	[i;neta;SQE;Cond_Num;e_validate]'
	% end

   % ==================================== %

	% ====================== %
        %   Stopping Criteria    %
  	% ====================== %

        % .... if stuck in a local minimum
	if reset_count>RESET_FREQ
		M=eye(nw);
		reset_count=1;

      if neta==0
		  stuck=stuck+1;
		end
                                
		if stuck>5
		   fprintf('\nstuck at local minimum\n');
	      loop=0;
		end
	else
      stuck=0;
   end

	% .... if SQE reduction is to small

	% Reset M if SQE reduction is poor
   % ==================================== %

	if (old_SQE-SQE)<old_SQE*0.0005
	   SQEcount=SQEcount+1;
	else
	  old_SQE=SQE;
	  SQEcount=1;
	end
    
        if SQEcount > SQE_BREAK
           loop=0;
           fprintf('Error reduction to small\n');
	end

	
	% .... if Error goal is achieved
	if SQE<COST_GOAL
	   loop=0;
	end

        %increment loop
      	i=i+1;
        
       	% ==================================== %
	% ==================================== %
  end
  
  % Form dlog
  % ==================================== %
 
  BBP_Update.dlog=[e_cost v_cost p_cost step_log MaxStep_log cond_log LSdata_log];
  fprintf('\n\n');
return

	


