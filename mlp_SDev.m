%
% [std_err, MSE] = mlp_SDev(network,TSu,TSy)
%
% the function computes the Mean Squared error and standard deviation in
% the square errors for an MLP network, where  
% the network object contains the weight information and flags are the MLP network parameters 
% u is the input matrix, y the output vector
% 
% Can make function compute the percentage mean square error
% instead by defining a global variable PMSEflag and setting it to 1



function [std_err, MSE] = mlp_SDev(network,TSu,TSy)
	global PMSEflag;
	[y ,r]=mlpnet(network,TSu);
 
	err=y{network.layers}-TSy;
	err=err.*err;
	std_err=std(err(:));
    
    MSE=mean(err(:));
	if ~isempty(PMSEflag)
	  if PMSEflag ==1
		MSE=MSE*100; % percentage squared error (assumes max output of 1);
	  end
	end
	

return
