%
% function [M] = bfgsfm(M,p,s)
%
% the function returns an estimate of the inverse Hessian by
% the full memory BFGS formula. p is the difference in the gradient
% vector and s is the difference in the parameter vector.
% p and s are column vectors.

% get the problem dimension

function [M] = bfgsfm(M,p,s)

n=size(p,1);

% calculate s'p and stop divide by zero
sp=s'*p;
if sp==0
	inv_sp=1/eps;
else
	sp=sign(sp)*(abs(sp)+eps);
	inv_sp=1/sp;
end

% bfgs formula
A = (1+inv_sp*p'*M*p)*s*s'*inv_sp;
B = - (s*p'*M+M*p*s')*inv_sp;
M=M+A+B;


